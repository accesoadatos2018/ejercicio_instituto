/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import model.Alumno;
import model.Nombre;
import model.Profesor;
import model.TipoFuncionario;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int opcion = 0;
        int dia, mes, anyo;
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        float saldo = 1000.0f;
        boolean registrado = true;
        //dia = c.get(Calendar.DAY_OF_MONTH);
        //mes = c.get(Calendar.MONTH);
        // anyo = c.get(Calendar.YEAR);
        //c.set(anyo, mes, dia);

        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        System.out.println("Cal: " + c.getTime());

        do {

            menu();
            opcion = s.nextInt();

            switch (opcion) {

                case 0: // Salir

                    System.out.println("Saliendo...");

                    break;

                case 1: // Insertar Alumno
                    //int id, String nombre, float sueldo, boolean registrado, Calendar fechaNac, Calendar horaTutoria, Calendar fechaRegistro, String observacione
                    Alumno a = new Alumno(1, "Tomas", saldo, registrado, c.getTime(), c.getTime(), c.getTime(), "Operación de pierna");

                    //Profesor p = new Profesor(new Nombre("Practicas", "Evaristo", "Facundo"), TipoFuncionario.PRACTICAS);
                    //CREAR UNA SESION
                    Session session = factory.openSession();
                    session.beginTransaction();

                    //GUARDAR OBJETO
                    session.saveOrUpdate(a);
                    //CERRAR CONEXION
                    session.getTransaction().commit();
                    session.close();
                    factory.close();

                    break;

                case 2: // Modificar Alumno

                    break;

                case 3: // Borrar Alumno

                    break;

                default:

                    System.out.println("Opción introducida NO Válida");

                    break;

            }

        } while (opcion != 0);

        //CREAMOS CONEXION
        //SessionFactory sessionFactory;
        //Configuration configuration = new Configuration();
        //configuration.configure();
        //sessionFactory = configuration.buildSessionFactory();
        //SessionFactory factory = new Configuration().configure().buildSessionFactory(); 
        /*
        // CREAMOS UN OBJETO
        //profesor profesor=new profesor(7,"Pepe","Garcia","Perez");
        
        //CREAR UNA SESION
        Session session=factory.openSession();
        session.beginTransaction();
        
        //GUARDAR OBJETO
        //session.save(profesor);
        
        //CERRAR CONEXION
        session.getTransaction().commit();
        session.close();
        factory.close();*/
    }

    public static void menu() {

        System.out.println("Selecciona opción");
        System.out.println("0. Salir");
        System.out.println("1. Crear Alumno");
        System.out.println("2. Modificar Alumno");
        System.out.println("3. Borrar Alumno");

    }

}
